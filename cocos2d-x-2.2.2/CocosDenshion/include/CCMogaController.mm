#include "CCMogaController.h"
#include "CCMogaController_objc.h"
#include "cocos2d.h"

USING_NS_CC;

static void static_end()
{
    [CCMogaController end];
}

static unsigned int static_getControllerState(int state)
{
    return [[CCMogaController shared] getControllerState: state];
}

static unsigned int static_getPlayerIndex()
{
    return [[CCMogaController shared] getPlayerIndex];
}

static unsigned int static_getControllerCount()
{
    return [[CCMogaController shared] getControllerCount];
}

static float static_getButtonValue(int keyCode)
{
    return [[CCMogaController shared] getButtonValue: keyCode];
}

static float static_getAxisValue(int axis)
{
    return [[CCMogaController shared] getAxisValue: axis];
}

namespace Moga {
    
    static CCMogaController *gameController;
    
    CCMogaController::CCMogaController()
    {
        
    }
    
    CCMogaController::~CCMogaController()
    {
        
    }
    
    CCMogaController* CCMogaController::shared()
    {
        if (! gameController)
        {
            gameController = new CCMogaController();
        }
        
        return gameController;
    }
    
    void CCMogaController::end()
    {
        if (gameController)
        {
            delete gameController;
            gameController = NULL;
        }
        
        static_end();
    }
    
    unsigned int CCMogaController::getControllerState(int state)
    {
        return static_getControllerState(state);
    }
    
    unsigned int CCMogaController::getPlayerIndex()
    {
        return static_getPlayerIndex();
    }
    
    unsigned int CCMogaController::getControllerCount()
    {
        return static_getControllerCount();
    }
    
    float CCMogaController::getButtonValue(int keyCode)
    {
        return static_getButtonValue(keyCode);
    }
    
    float CCMogaController::getAxisValue(int axis)
    {
        return static_getAxisValue(axis);
    }
    
} // endof namespace Moga